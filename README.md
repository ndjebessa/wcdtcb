wcdTCB
======

Weakly-coupled, distributed trusted computing bases for making security measurable through trustworthiness.
